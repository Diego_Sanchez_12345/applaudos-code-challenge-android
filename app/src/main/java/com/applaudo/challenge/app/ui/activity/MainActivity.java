package com.applaudo.challenge.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import com.applaudo.challenge.app.R;
import com.applaudo.challenge.app.ui.fragment.AnimeDetailFragment;
import com.applaudo.challenge.app.ui.fragment.CategoryListFragment;
import com.applaudo.challenge.app.utils.Const;
import com.applaudo.challenge.app.utils.interfaces.OnRequestStatus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements OnRequestStatus{

    public static void open(Activity activity) {
        activity.startActivity(new Intent(activity, MainActivity.class));
    }

    private boolean isAnimeDetail;
    private ActionBar actionBar;

    @BindView(R.id.swrLoader)
    SwipeRefreshLayout swrLoader;
    @BindView(R.id.contFragment)
    RelativeLayout rlContFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            actionBar = getSupportActionBar();
        }
        swrLoader.setOnRefreshListener(null);
        loaderStatus(false);
        isAnimeDetail = false;
        CategoryListFragment categoryListFragment = new CategoryListFragment();
        loadFragment(categoryListFragment);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                isAnimeDetail = false;
                CategoryListFragment categoryListFragment = new CategoryListFragment();
                loadFragment(categoryListFragment);
                if (actionBar != null) {
                    actionBar.setDisplayHomeAsUpEnabled(false);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void loadDetailAnime(int idAnime) {
        Bundle b = new Bundle();
        b.putInt(Const.EXTRA_BUNDLE_DETAIL, idAnime);
        isAnimeDetail = true;
        AnimeDetailFragment animeDetailFragment = new AnimeDetailFragment();
        loadFragment(animeDetailFragment, b, true);
    }

    private void loadFragment(Fragment fragment, Bundle b, boolean data) {

        if (actionBar != null) {
            if (isAnimeDetail) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(getResources().getString(R.string.app_name_anime_detail));
            } else {
                actionBar.setTitle(getResources().getString(R.string.app_name_anime));
            }
        }

        if (data) {
            fragment.setArguments(b);
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        List<Fragment> fragments = fm.getFragments();
        if (fragments.isEmpty()) {
            ft.add(R.id.contFragment, fragment);
            ft.commit();
        } else {
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_out_right, android.R.anim.slide_in_left);
            ft.replace(R.id.contFragment, fragment);
            ft.commit();
        }
    }

    public void loadFragment(Fragment fragment) {
        loadFragment(fragment, null, false);
    }

    public void loaderStatus(boolean status){
        if (status){
            swrLoader.setEnabled(status);
            swrLoader.setRefreshing(status);
        }else {
            swrLoader.setRefreshing(status);
            swrLoader.setEnabled(status);
        }
    }

    @Override
    public void onStartRequest() {
        loaderStatus(true);
    }

    @Override
    public void onFinishRequest() {
        loaderStatus(false);
    }

    @Override
    public void onErrorRequest(String error) {
        loaderStatus(false);
        Snackbar.make(rlContFragment,error,Snackbar.LENGTH_SHORT).show();
    }
}
