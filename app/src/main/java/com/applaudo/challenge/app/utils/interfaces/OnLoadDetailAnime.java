package com.applaudo.challenge.app.utils.interfaces;

public interface OnLoadDetailAnime {
    void onLoadDetail(int idAnime);
}
