package com.applaudo.challenge.app.utils.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applaudo.challenge.app.R;
import com.applaudo.challenge.app.model.Anime;
import com.applaudo.challenge.app.utils.interfaces.OnItemAnimeClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterAnime extends RecyclerView.Adapter<AdapterAnime.AnimeViewHolder> {

    Context context;
    LayoutInflater inflater;
    private List<Anime> animes;
    private OnItemAnimeClick listener;

    public AdapterAnime(Context context){
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public AdapterAnime.AnimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_anime_list,parent,false);
        return new AnimeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterAnime.AnimeViewHolder holder, int position) {
        Anime anime = animes.get(position);
        holder.txvTitleAnime.setText(anime.getAttributes().getTitles().getEn()!=null?
                anime.getAttributes().getTitles().getEn():anime.getAttributes().getTitles().getEn_jp());
        Glide.with(context).load(anime.getAttributes().getPosterImage().getTiny()).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)).into(holder.imvIconAnime);
    }

    @Override
    public int getItemCount() {
        return animes.size();
    }

    public void setData(List<Anime> animes){
        this.animes = animes;
        notifyDataSetChanged();
    }

    public void setListener(OnItemAnimeClick listener) {
        this.listener = listener;
    }

    public class AnimeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txvTitleAnime) TextView txvTitleAnime;
        @BindView(R.id.imvIconAnime) ImageView imvIconAnime;
        @BindView(R.id.itemAnime) RelativeLayout lyParent;

        public AnimeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            lyParent.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClickItem(animes.get(getAdapterPosition()).getId());
        }
    }
}
