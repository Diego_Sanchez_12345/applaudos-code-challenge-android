package com.applaudo.challenge.app.utils.interfaces;

import com.applaudo.challenge.app.api.Response;
import com.applaudo.challenge.app.model.Anime;
import com.applaudo.challenge.app.model.Category;
import com.applaudo.challenge.app.model.Genres;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ServiceApi {

    @GET("categories/{idCategory}/anime")
    Call<Response<List<Anime>>> getAnimeList(@Path("idCategory") int idCategory);

    @GET("categories")
    Call<Response<List<Category>>> getCategorys();

    @GET("anime/{idAnime}")
    Call<Response<Anime>> getAnime(@Path("idAnime") int idAnime);

    @GET("anime/{idAnime}/genres")
    Call<Response<List<Genres>>> getGenresByAnime(@Path("idAnime") int idAnime);

}
