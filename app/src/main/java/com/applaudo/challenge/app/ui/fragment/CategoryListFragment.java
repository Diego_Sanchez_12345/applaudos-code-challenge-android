package com.applaudo.challenge.app.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applaudo.challenge.app.R;
import com.applaudo.challenge.app.api.Response;
import com.applaudo.challenge.app.api.RetrofitClient;
import com.applaudo.challenge.app.model.Category;
import com.applaudo.challenge.app.ui.activity.MainActivity;
import com.applaudo.challenge.app.utils.adapter.AdapterCategory;
import com.applaudo.challenge.app.utils.interfaces.OnLoadDetailAnime;
import com.applaudo.challenge.app.utils.interfaces.OnRequestStatus;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryListFragment extends Fragment implements OnLoadDetailAnime {

    private View rootView;

    public CategoryListFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.rcvCategory) RecyclerView rcvCategory;
    AdapterCategory adapterCategory;
    MainActivity activity;
    OnRequestStatus listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_category_list, container, false);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
        listener = (OnRequestStatus) context;
    }
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            this.activity =(MainActivity) activity;
            listener = (OnRequestStatus) activity;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,rootView);

        adapterCategory = new AdapterCategory(getActivity());
        adapterCategory.setListener(this);
        rcvCategory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvCategory.setAdapter(adapterCategory);
        listener.onStartRequest();
        RetrofitClient.getService().getCategorys().enqueue(new Callback<Response<List<Category>>>() {
            @Override
            public void onResponse(@NonNull Call<Response<List<Category>>> call, @NonNull retrofit2.Response<Response<List<Category>>> response) {
                Response<List<Category>> responseCategory = response.body();
                if (responseCategory!=null) {
                    adapterCategory.setData(responseCategory.getData());
                    listener.onFinishRequest();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response<List<Category>>> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(),t.getMessage(),Toast.LENGTH_LONG).show();
                Log.d("ERROR",t.getMessage());
                listener.onErrorRequest(t.getMessage());
            }
        });
    }

    @Override
    public void onLoadDetail(int idAnime) {
        activity.loadDetailAnime(idAnime);
    }
}
