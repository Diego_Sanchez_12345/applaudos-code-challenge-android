package com.applaudo.challenge.app.utils.interfaces;

public interface OnItemAnimeClick {
    void onClickItem(int idAnime);
}
