package com.applaudo.challenge.app.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.applaudo.challenge.app.R;
import com.applaudo.challenge.app.api.Response;
import com.applaudo.challenge.app.api.RetrofitClient;
import com.applaudo.challenge.app.model.Anime;
import com.applaudo.challenge.app.model.Genres;
import com.applaudo.challenge.app.ui.activity.MainActivity;
import com.applaudo.challenge.app.utils.Const;
import com.applaudo.challenge.app.utils.interfaces.OnRequestStatus;
import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnimeDetailFragment extends Fragment{


    public AnimeDetailFragment() {
        // Required empty public constructor
    }

    private MainActivity activity;
    private View rootView;
    private YouTubePlayer youtTubePlayer;
    OnRequestStatus listener;
    @BindView(R.id.contMain) RelativeLayout rlMain;
    @BindView(R.id.txvAnimeTitle) TextView txvAnimeTitle;
    @BindView(R.id.txvCanonicalTitle) TextView txvCanonicalTitle;
    @BindView(R.id.txvShowType) TextView txvShowType;
    @BindView(R.id.txvNumberEp) TextView txvNumberEp;
    @BindView(R.id.txvStartDate) TextView txvStartDate;
    @BindView(R.id.txvEndDate) TextView txvEndDate;
    @BindView(R.id.txvGenres) TextView txvGenres;
    @BindView(R.id.txvAverageRating) TextView txvAverageRating;
    @BindView(R.id.txvEpisodeDuration) TextView txvEpisodeDuration;
    @BindView(R.id.txvAgeRating) TextView txvAgeRating;
    @BindView(R.id.txvAiringStatus) TextView txvAiringStatus;
    @BindView(R.id.txvSynopsis) TextView txvSynopsis;
    @BindView(R.id.imvAnime) ImageView imvAnime;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_anime_detail, container, false);
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
        listener = (OnRequestStatus) context;
    }
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            this.activity =(MainActivity) activity;
            listener = (OnRequestStatus) activity;
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,rootView);
        Bundle bundle = getArguments();
        if (bundle!=null) {
            listener.onStartRequest();
            RetrofitClient.getService().getAnime(bundle.getInt(Const.EXTRA_BUNDLE_DETAIL)).enqueue(new Callback<Response<Anime>>() {
                @Override
                public void onResponse(@NonNull Call<Response<Anime>> call, @NonNull retrofit2.Response<Response<Anime>> response) {
                    Response<Anime> animeResponse = response.body();
                    if (animeResponse!=null) {
                        Anime anime = animeResponse.getData();
                        if (getActivity()!=null) {
                            Glide.with(getActivity())
                                    .load(anime.getAttributes().getPosterImage().getSmall())
                                    .thumbnail(0.5f)
                                    .into(imvAnime);
                        }
                        txvAnimeTitle.setText(anime.getAttributes().getTitles().getEn()!=null?
                                anime.getAttributes().getTitles().getEn():anime.getAttributes().getTitles().getEn_jp());
                        txvCanonicalTitle.setText(anime.getAttributes().getCanonicalTitle());
                        txvShowType.setText(anime.getAttributes().getShowType());
                        txvNumberEp.setText(String.valueOf(anime.getAttributes().getEpisodeCount()));
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/mm/dd");
                        txvStartDate.setText(simpleDateFormat.format(anime.getAttributes().getStartDate()));
                        txvEndDate.setText(simpleDateFormat.format(anime.getAttributes().getEndDate()));
                        txvAverageRating.setText(anime.getAttributes().getAverageRating());
                        txvEpisodeDuration.setText(String.valueOf(anime.getAttributes().getEpisodeLength()));
                        txvAgeRating.setText(anime.getAttributes().getAgeRating());
                        txvAiringStatus.setText(anime.getAttributes().getStatus());
                        txvSynopsis.setText(anime.getAttributes().getSynopsis());
                        rlMain.setVisibility(View.VISIBLE);
                        loadYouTubeVideo(anime.getAttributes().getYoutubeVideoId());
                        listener.onFinishRequest();

                        RetrofitClient.getService().getGenresByAnime(anime.getId()).enqueue(new Callback<Response<List<Genres>>>() {
                            @Override
                            public void onResponse(@NonNull Call<Response<List<Genres>>> call, @NonNull retrofit2.Response<Response<List<Genres>>> response) {
                                Response<List<Genres>> genresResponse = response.body();
                                if (genresResponse!=null){
                                    StringBuilder genresList = new StringBuilder();
                                    for (int i=0;i<genresResponse.getData().size();i++){
                                        Genres genres = genresResponse.getData().get(i);
                                        if (i+1<genresResponse.getData().size()) {
                                            genresList.append(genres.getAttributes().getName()).append(",");
                                        }else {
                                            genresList.append(genres.getAttributes().getName());
                                        }
                                    }
                                    txvGenres.setText(genresList.toString());
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<Response<List<Genres>>> call, @NonNull Throwable t) {
                                txvGenres.setText("--");
                            }
                        });
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Response<Anime>> call, @NonNull Throwable t) {
                    listener.onErrorRequest(t.getMessage());
                }
            });
        }
    }

    public void loadYouTubeVideo(final String youTubeVideoId) {

        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtubePlayer, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize("DEVELOPER_KEY", new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youtTubePlayer = youTubePlayer;
                    youtTubePlayer.cueVideo(youTubeVideoId);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

    }

}
