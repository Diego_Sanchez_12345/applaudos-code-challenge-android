package com.applaudo.challenge.app.utils.interfaces;

/**
 * Created by Diego-Montes on 23/7/2018.
 */

public interface OnRequestStatus {
    void onStartRequest();
    void onFinishRequest();
    void onErrorRequest(String error);
}
