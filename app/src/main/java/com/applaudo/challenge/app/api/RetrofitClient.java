package com.applaudo.challenge.app.api;

import com.applaudo.challenge.app.utils.interfaces.ServiceApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient<T> {

    private static String URL_SERVER =  "https://kitsu.io/api/edge/";
    private static Retrofit retrofit = null;

    private static Retrofit getClient(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL_SERVER)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static ServiceApi getService(){
        return getClient().create(ServiceApi.class);
    }


}
