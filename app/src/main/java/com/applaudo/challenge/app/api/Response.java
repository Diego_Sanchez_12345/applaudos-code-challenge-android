package com.applaudo.challenge.app.api;

import com.applaudo.challenge.app.model.Link;
import com.applaudo.challenge.app.model.Meta;


public class Response<T> {
    private T data;
    private Meta meta;
    private Link links;

    public Response() {
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Link getLinks() {
        return links;
    }

    public void setLinks(Link links) {
        this.links = links;
    }
}
