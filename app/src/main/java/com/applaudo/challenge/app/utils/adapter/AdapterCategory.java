package com.applaudo.challenge.app.utils.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.applaudo.challenge.app.R;
import com.applaudo.challenge.app.api.Response;
import com.applaudo.challenge.app.api.RetrofitClient;
import com.applaudo.challenge.app.model.Anime;
import com.applaudo.challenge.app.model.Category;
import com.applaudo.challenge.app.utils.interfaces.OnItemAnimeClick;
import com.applaudo.challenge.app.utils.interfaces.OnLoadDetailAnime;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.CategoryViewHolder> implements OnItemAnimeClick {

    private LayoutInflater inflater;
    private Context context;
    private List<Category> categories;



    private OnLoadDetailAnime listener;

    public AdapterCategory(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        categories = new ArrayList<>();
    }

    @NonNull
    @Override
    public AdapterCategory.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_category_list,parent,false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterCategory.CategoryViewHolder holder, int position) {
        Category category = categories.get(position);
        holder.txvCategory.setText(category.getAttributes().getTitle());
        final AdapterAnime adapterAnime = new AdapterAnime(context);
        adapterAnime.setListener(this);
        RetrofitClient.getService().getAnimeList(category.getId()).enqueue(new Callback<Response<List<Anime>>>() {
            @Override
            public void onResponse(@NonNull Call<Response<List<Anime>>> call, @NonNull retrofit2.Response<Response<List<Anime>>> response) {
                Response<List<Anime>> responseAnime = response.body();
                if (responseAnime!=null){
                    adapterAnime.setData(responseAnime.getData());
                    holder.rcvAnimeList.setAdapter(adapterAnime);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Response<List<Anime>>> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    @Override
    public void onClickItem(int idAnime) {
        listener.onLoadDetail(idAnime);
    }

    public void setListener(OnLoadDetailAnime listener) {
        this.listener = listener;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txvCategory) TextView txvCategory;
        @BindView(R.id.rcvAnimeList) RecyclerView rcvAnimeList;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            rcvAnimeList.setLayoutManager(new GridLayoutManager(context,1,GridLayoutManager.HORIZONTAL,false));
        }
    }

    public void setData(List<Category> categories){
        this.categories = categories;
        notifyDataSetChanged();
    }
}
